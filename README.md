# Ambiente Flutter no Docker

Projeto com ambiente básico para desenvolvimento Flutter no docker, contém:
- Arquivo [Dockerfile](Dockerfile) para geração de imagem docker ambiente de desenvolvimento Flutter,  
com configuração para trabalhar com Flutter Web também;
- Arquivo [docker-compose](docker-compose.yml) para execução de container com imagem criada 
e com volume mapeado em pasta [workspace](workspace) para manter arquivos salvo quando apagar container;
- Container do [portainer](https://www.portainer.io/) no [docker-compose](docker-compose.yml) para administração dos container Docker;
- Documentação e comandos utéis para adminstraçao do ambiente e projetos flutter.

Baseado nos seguintes repositórios:
- Ambiente Flutter no Docker: [cbsan/flutter](https://github.com/cbsan/flutter),
- Ambiente Flutter Web no Docker: [jared-nelsen/flutter_web_docker_example](https://github.com/jared-nelsen/flutter_web_docker_example).

### Comandos Docker

Obs.: [OPCIONAL] Siga o passo "Utilização/Compilação" para gerar build de uma imagem do Android SDK antes de tudo 
baixando o repositório [Ambiente Android SDK Docker](https://gitlab.com/ReinaldoDomingos/ambiente-android-sdk-docker).

#### Passo 0 - Gerar build imagem docker ambiente Flutter
Obs.: Para gerar a imagem do ambiente Flutter é necessário ter uma imagem do [Ambiente do Android SDK](https://gitlab.com/ReinaldoDomingos/ambiente-android-sdk-docker)
ou substituir a seguinte linha no arquivo [Dockerfile](Dockerfile):

`FROM reijunior100/android-sdk`

Por essa:

`FROM cbsan/android-sdk`

É necessário que a imagem _cbsan/android-sdk_ ainda tenha registro no [Docker hub](https://hub.docker.com/r/cbsan/android-sdk)
para ser possível usar essa alternativa, por isso existe o procedimento de build manual no início desse texto 
caso não consiga dessa forma.

O comando abaixo é necessário ser executado somente nos seguintes cenários:
- 1° vez para criar imagem docker para ser usada no docker compose;
- Quando alterar algo no arquivo [Dockerfile](Dockerfile);
- Ou quando apagar a imagem no docker.


  `docker build -t reijunior100/flutter .`

#### Passo 1 - Executar projeto no docker e criar containers:

  `docker-compose up -d`

#### Passo 2 - Acessar terminal bash no container ubuntu:

  `docker-compose exec app bash`

#### Passo 3 - Parar containers do projeto no docker:

Esse comando deve ser executado sempre que parar o desenvolvimento para
parar os container docker e liberar memória RAM do computador.

  `docker-compose stop`

#### Apagar containers do projeto no docker:

Obs.: Esse comando não apagará seus aplicativos e demais arquivos que 
estiver na pasta workspace, pois ela existe fora do container também.

**Atenção:** Será apagado o cache dos pacotes Flutter, Dart, Gradle,
SDKs e emuladores das plataformas, o que tornará mais demorada a próxima
execução e compilação de um aplicativo flutter. Qualquer configuração adicional
feita no ambiente após a execução também será apagada.

  `docker-compose down`

### Comandos  Flutter

Obs.: Esses comandos devem ser executados via terminal bash do container.

#### Verificar dependências Flutter:

  `flutter doctor`

#### Atualizar flutter (terminal container):

  `flutter upgrade`

#### Compilar e executar seu aplicativo mobile
- Exemplo compilar e executar em qualquer dispositivo conectado: `flutter run`.
- Exemplo compilar e executar no android:`flutter run android`.

Obs.: Uma vez executado seu aplicativo após alterações pode digitar a tecla 'r'
para aproveitar o _Hot reload_ do Flutter e não precisar executar novamente.

Caso não tenha um dispostivo conectado ou queira apenas compilar:

`flutter build <nome_plataforma>`

#### Compilar e executar seu aplicativo versão web
- Passo 1 - Compilar aplicativo para web: `flutter build web`
- Passo 2 - Copie o arquivo [server.sh](workspace/server.sh) para dentro 
da pasta do aplicativo que queira executar. Exemplo: `workspace/demo/server.sh`
- Passo 3 - Entre na pasta do seu aplicativo via terminal do seu usuário ou do container. Exemplo:  `cd workspace/demo/`
- Passo 4 - Para criar um servidor web simples execute `./server.sh`.
- Passo 5 - Acesse seu aplicativo na url http://localhost:4040.

Observações:
- O servidor encerra com o comando _Ctrl + c_;
- Na versão atual do Flutter web, mesmo parando o servidor a aplicação continuará
disponível na url offline, devido uso de tecnologia chamada [Service Worker](https://developer.mozilla.org/pt-BR/docs/Web/API/Service_Worker_API/Using_Service_Workers)
e [Cache Storage](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorage);
- Para que a aplicação apague da url, limpe completamente o cache do navegador nessa url;
- O _Hot reload_ do Flutter ainda não funciona na versão web;
- Caso queira altere a porta do servidor no arquivo server.sh interno no seu aplicativo,
podendo customizar por exemplo uma porta para cada aplicativo web para rodar ao mesmo tempo caso queira.
- Para executar servidor web dentro do container para um aplicativo com porta diferente da 4040 tem que 
mapear essa porta no arquivo [docker-compose](docker-compose.yml) no serviço Flutter
ou executar fora do container.

### Comandos bash úteis

#### Adicionar permissão na pasta do seu aplicativo para seu usuário:
 
Geralmente como a pasta foi criada pelo usuário Docker dentro do container pode ser 
que seu usuário não tenha permissão de modificá-la inicialmente.

`sudo chmod 777 pasta_app`

© Copyright 2021 - All rights reserved | Todos os direitos Reservados

**Reinaldo Domingos de Freitas Junior**